import "reflect-metadata";
import { createConnection } from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import { Request, Response } from "express";
import { Routes } from "./routes";
import * as cors from "cors";
import * as fileUpload from "express-fileUpload";

createConnection().then(async connection => {
    const app = express();
    
    app.use(fileUpload())
    app.use(bodyParser.json());
    app.use(cors());
    app.set('env', process.env.APP_ENV);

    Routes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    app.listen(process.env.PORT);
    console.log("Express server has started on port 3001. Open http://localhost:3001/upload to see results");

}).catch(error => console.log(error));
