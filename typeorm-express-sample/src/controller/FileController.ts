import { NextFunction, Request, Response } from "express";
import { MnistData } from '../data';
import * as Script from "../script.js"

export class FileController {
    async upload(request: Request, response: Response, next: NextFunction) {
        console.log(request.body);
        response.status(200);
        return "File uploaded succesfully!";
    }

    async evaluate(request: Request, response: Response, next: NextFunction) {
        if (request.files == null)
        {
            response.status(404);
            return "No files were sent :(";
        }

        try {
            const file = request.files.file;
            const result = await Script.classify(file.data);
            response.status(200);
            return result;
        }
        catch(err) {
            response.status(404);
            return err;
        }
    }

    async train(request: Request, response: Response, next: NextFunction) {
        const model = await Script.loadModel();
        const data = new MnistData();
        await data.load();

        const newModel = await Script.train(model, data);
        await Script.saveModel(newModel);

        response.status(200);
        return "Train finished succesfully!";
    }
}