import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { User } from "../entity/User";
import { AuthToken } from "../middleware/AuthToken";

export class UserController {

    private userRepository = getRepository(User);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.find();
    }

    async one(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.findOne(request.params.id);
    }

    async exist(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.findOne(request.params.id);
    }

    async findByName(request: Request, response: Response, next: NextFunction) {
        let name = request.params.firstName;

        return this.userRepository.findOne({
            where: {
                firstName: name
            }
        });
    }

    async findAllByName(request: Request, response: Response, next: NextFunction) {
        let name = request.params.firstName;

        return this.userRepository.find({
            where: {
                firstName: name
            }
        });
    }

    async save(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.save(request.body);
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let userToRemove = await this.userRepository.findOne(request.params.id);
        await this.userRepository.remove(userToRemove);
    }

    async update(request: Request, response: Response, next: NextFunction) {
        let user = await this.userRepository.findOne(request.params.id);
        const body = request.body;

        user.username = body.username;
        user.email = body.email;
        user.password = body.password;

        return await this.userRepository.save(user);
    }

    async login(request: Request, response: Response, next: NextFunction) {
        const body = request.body;
        const userMail = body.email;
        const userPass = body.password;

        let user = await this.userRepository.findOne({
            where: {
                email: userMail,
                password: userPass
            }
        });

        if (!user) {
            response.status(404);
            return "Incorrect email or password";
        }

        response.status(200);
        return AuthToken(user.id, user.username);
    }

    async signUp(request: Request, response: Response, next: NextFunction) {
        const body = request.body;
        const userMail = body.email;
        const userPass = body.password;
        const userName = body.username;

        let user = await this.userRepository.findOne({
            where: {
                email: userMail,
                username: userName
            }
        });

        if (user) {
            response.status(404);
            return "Invalid user(an user with this name already exist)!";
        }

        this.userRepository.save(request.body);

        return "New user created!";
    }

}