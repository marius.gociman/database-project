import { getRepository } from "typeorm";
import { NextFunction, Request, Response } from "express";
import { Result } from "../entity/Result";

export class ResultController {
    private resultRepository = getRepository(Result);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.resultRepository.find();
    }

    async addResult(request: Request, response: Response, next: NextFunction) {
        return this.resultRepository.save(request.body);
    }
}