import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Result {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    result: number;

    @Column()
    size: number;
}
