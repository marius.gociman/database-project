import { UsingJoinTableIsNotAllowedError } from "typeorm";

var jwt = require('jsonwebtoken');

export function AuthToken(userId, userName){
    return VerifyToken(jwt.sign({id: userId, user: userName}, process.env.SECRET_KEY));
}

export function VerifyToken(token){
    if(jwt.verify(token, process.env.SECRET_KEY)){
        return token;
    }
    return "Token not valid!";
}