// https://us-central1-causal-hour-334314.cloudfunctions.net/createSchedule
exports.createSchedule = (req, res) => {
  res.type('application/json');
  res.set('Access-Control-Allow-Origin', "*");
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Credentials", "true");
  res.set("Access-Control-Allow-Methods", "GET, POST, PUT");
  res.set("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");

  if (req.method === "OPTIONS") {
    // stop preflight requests here
    res.status(204).send('No Content');
    return;
  }

  const kindName = 'schedule';
  const { Datastore } = require("@google-cloud/datastore");
  const moment = require("moment");
  const start = req.body.start_date;
  const end = req.body.end_date;

  const datastore = new Datastore({
    projectId: "causal-hour-334314",
    keyFilename: "./credentials.json"
  });

  if (!moment(start, 'YYYY-MM-DD').isValid() || !moment(end, 'YYYY-MM-DD').isValid())
  {
    res.status(500).send("The dates are invalid.");
    return;
  }

  if(moment(start).isAfter(end)){
    res.status(500).send("The end date is before the start date.");
    return;
  }

  datastore
    .save({
      key: datastore.key(kindName),
      data: {
        start_date: start,
        end_date: end
      }
    })
    .catch(err => {
        console.error('ERROR:', err);
        res.status(500).send(JSON.stringify(err));
        return;
    });

    const schedule = {start, end};
    res.status(200).send(JSON.stringify({"message": "Succesfully created a new schedule!"}));
};

// https://us-central1-causal-hour-334314.cloudfunctions.net/getSchedules
exports.getSchedules = (req, res) => {
  const kindName = 'schedule';
  const { Datastore } = require("@google-cloud/datastore");
  const datastore = new Datastore({
    projectId: "causal-hour-334314",
    keyFilename: "./credentials.json"
  });

  res.type('application/json');
  res.set('Access-Control-Allow-Origin', "*")
  res.set('Access-Control-Allow-Methods', 'GET');

  if (req.method === "OPTIONS") {
    // stop preflight requests here
    res.status(204).send('No Content!');
    return;
  }

	const queryString = kindName;
  const query = datastore.createQuery(queryString);

  datastore.runQuery(query)
    .then(task => {
      console.log(task[0]);
      res.status(200).send(JSON.stringify(task[0]));
    })
    .catch(err => {
      res.status(500).send(JSON.stringify(err));
      return;
    });
};